import { NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './modules/material/material.module';

import { AppComponent } from './app.component';
import { SnackBarComponent } from './components/snack-bar/snack-bar.component';
import { LoaderComponent } from './components/loader/loader.component';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProfileComponent } from './components/profile/profile.component';
import { ReportVehicleComponent } from './components/report-vehicle/report-vehicle.component';
import { LiveMapComponent } from './components/live-map/live-map.component';
import { VehicleOverviewComponent } from './components/vehicle-overview/vehicle-overview.component';


import { HttpClientService } from './services/http-client/http-client.service';
import { AccountService } from './services/account/account.service';
import { AuthGuardService } from './services/auth-guard/auth-guard.service';
import { SnackBarService } from './services/snack-bar/snack-bar.service';
import { LoaderService } from './services/loader/loader.service';
import { MovementService} from './services/movement/movement-service';
import {VehicleService} from './services/vehicle-service/vehicle-service';
import {OwnerService} from './services/owner-service/owner-service';
import { VehicleHistoryComponent } from './components/vehicle-history/vehicle-history.component';


@NgModule({
  declarations: [
    AppComponent,
    SnackBarComponent,
    LoaderComponent,
    LoginComponent,
    DashboardComponent,
    ProfileComponent,
    ReportVehicleComponent,
    LiveMapComponent,
    VehicleOverviewComponent,
    VehicleHistoryComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    MaterialModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [
    HttpClientService,
    AccountService,
    AuthGuardService,
    SnackBarService,
    LoaderService,
    VehicleService,
    OwnerService,
    MovementService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}
