import { Injectable } from '@angular/core';
import {Vehicle} from '../../models/Vehicle';
import {HttpClientService} from '../http-client/http-client.service';
import {Observable} from 'rxjs/Observable';
import {TransLocation} from '../../models/TransLocation';
import {BASE_WS_URL} from '../../utils/Constants';

@Injectable()
export class MovementService {

  trackedVehicles: String[] = [];
  storedTransLocations: TransLocation[] = [];

  ws: WebSocket;

  constructor(private http: HttpClientService) {
    this.getAllTracedCarTrackers().subscribe(
      data => this._parseTrackIDs(data),
      error => {},
      () => {}
    )
  }

  initSocket() :Observable<TransLocation> {
    this.ws = new WebSocket(BASE_WS_URL);
    //this.ws.send(this.sendVehicleHistorySignal);
    console.log("connected");

    return new Observable(observer => {
      this.ws.onmessage = (event) => observer.next(this._parseTransLocation(event.data));
      this.ws.onerror = (event) => observer.error(event);
      this.ws.onclose = (event) => {observer.complete(), console.log("closed websocket connection")};
    })
  }


  _parseTransLocation(transLocationJson: any) {
    let transLocation: TransLocation = JSON.parse(transLocationJson);
    //this.storedTransLocations.push(transLocation);

    //console.log("comming from " + transLocation.countryCode);
    return transLocation;
  }

  _parseTrackIDs(data: any){
    for (let trackID in data){
      this.trackedVehicles.push(data[trackID]);
    }
  }


  giveTrackingSignal(carTracker: string){
    if (!this.trackedVehicles.includes(carTracker)) {
      this.trackedVehicles.push(carTracker);
    }

    return this.http
      .post("vehicle/add/" + carTracker, carTracker);
  }

  giveSTOPTrackingSignal(carTracker: string){
    if (this.trackedVehicles.includes(carTracker)) {
      this.trackedVehicles.splice(this.trackedVehicles.indexOf(carTracker));
    }

    return this.http
      .post("vehicle/remove/" + carTracker , carTracker);

  }

  getAllTracedCarTrackers(){
    return this.http
      .get("vehicle/track");
  }

  removeFromBuffer(carTracker: string){
    this.ws.send(carTracker);
  }




}
