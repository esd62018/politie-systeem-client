import { Injectable } from '@angular/core';
import {HttpClientService} from '../http-client/http-client.service';
import {Observable} from 'rxjs/Observable';
import {Owner} from '../../models/Owner';

@Injectable()
export class OwnerService {

  constructor(
    private http: HttpClientService,
  ) { }


  getOwner(licensePlate : string) : Observable<Owner> {
    return this.http
      .get<Owner>("owners/by-license-plate/" + licensePlate)
  }


}
