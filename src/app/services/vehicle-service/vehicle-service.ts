import { Injectable } from '@angular/core';
import {HttpClientService} from '../http-client/http-client.service';
import {LoaderService} from '../loader/loader.service';
import {Router} from '@angular/router';
import {SnackBarService} from '../snack-bar/snack-bar.service';
import {Vehicle} from '../../models/Vehicle';
import {Observable} from 'rxjs/Observable';
import { BASE_API_URL } from '../../utils/Constants';
import {HttpClient, HttpErrorResponse} from '@angular/common/http';
import {Account} from '../../models/Account';
import * as AuthStore from '../../utils/AuthStore';
import {tap} from 'rxjs/operators';

@Injectable()
export class VehicleService {

  constructor(
    private http: HttpClientService
  ) { }

  getStolenVehicles() : Observable<Vehicle[]> {
    return this.http
      .get<Vehicle[]>("vehicle/stolen")
  }

  postStolenVehicle(vehicle: Vehicle) {
    return this.http
      .post<Vehicle>( "vehicle/report", vehicle)
  }

  reportAsFound(carTracker: string) {
    return this.http
      .delete("vehicle/reportAsFound/" + carTracker)
  }



  errorHandler(error: HttpErrorResponse){
    console.log("[VehicleService] " + error.message);
  }


}



