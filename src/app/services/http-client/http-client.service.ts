import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { BASE_API_URL } from '../../utils/Constants';

const headers: any = {
  'Accept': 'application/json',
  'Content-Type': 'application/json'
};

@Injectable()
export class HttpClientService {
  constructor(private http: HttpClient) {}

  static setTokenHeader(token: string): void {
    headers.token = token;
  }

  static clearTokenHeader(): void {
    delete headers.token;
  }

  get<T>(url) {
    return this.http.get<T>(`${BASE_API_URL}${url}`, { headers: new HttpHeaders(headers) });
  }

  post<T>(url, data) {
    return this.http.post<T>(`${BASE_API_URL}${url}`, data, { headers: new HttpHeaders(headers) });
  }

  put<T>(url, data) {
    return this.http.put<T>(`${BASE_API_URL}${url}`, data, { headers: new HttpHeaders(headers) });
  }

  delete<T>(url) {
    return this.http.delete<T>(`${BASE_API_URL}${url}`, { headers: new HttpHeaders(headers) });
  }
}
