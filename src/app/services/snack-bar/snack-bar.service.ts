import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class SnackBarService {
  private _messageSubject: Subject<string>;
  private _message: Observable<string>;

  constructor() {
    this._messageSubject = new Subject<string>();
    this._message = this._messageSubject.asObservable();
  }

  get message(): Observable<string> {
    return this._message;
  }

  showSnackBar(message: string, delay: number = 2500) {
    this._messageSubject.next(message);

    this.clearSnackBar(delay);
  }

  showOops() {
    this._messageSubject.next('Oops, something went wrong');
    //this._messageSubject.next(input);

    this.clearSnackBar(2500);
  }

  clearSnackBar(delay: number): void {
    setTimeout(() => {
      this._messageSubject.next(null);
    }, delay);
  }
}
