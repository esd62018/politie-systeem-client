import {Vehicle} from './Vehicle';

export class Owner {
  public citizenServiceNumber : string;
  public country: string;
  public firstName: string;
  public lastName: string;
  public city: string;
  public zipCode: string;
  public street: string;
  public houseNumber: string;
  public stolenVehicles: Vehicle[];

  constructor(citizenServiceNumber: string, country: string, firstName: string, lastName: string, city: string, zipCode: string, street: string, houseNumber: string) {
    this.citizenServiceNumber = citizenServiceNumber;
    this.country = country;
    this.firstName = firstName;
    this.lastName = lastName;
    this.city = city;
    this.zipCode = zipCode;
    this.street = street;
    this.houseNumber = houseNumber;


    this.stolenVehicles = [];
  }
}
