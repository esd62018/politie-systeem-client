

export class TransLocation {
  public serialNumber : string;
  public lat: number;
  public lon: number;
  public dateTime: string;
  public countryCode: string;


  constructor(serialNumber: string, lat: string, lon: string, dateTime: string, countryCode: string) {
    this.serialNumber = serialNumber;
    this.lat = Number(lat);
    this.lon = Number(lon);
    this.dateTime = dateTime;
    this.countryCode = countryCode;
  }
}
