export class Account {
  public id?: number = null;
  public userName?: string = null;
  public password?: string = null;
  public street?: string = null;
  public houseNumber?: string = null;
  public zipCode?: string = null;
  public city?: string = null;
  public country?: string = null;
  public firstName?: string = null;
  public lastName?: string = null;


  constructor(obj: Account = {} as Account) {
    const {
      id,
      userName,
      password,
      street,
      houseNumber,
      zipCode,
      city,
      country,
      firstName,
      lastName
    } = obj;

    this.id = id;
    this.userName = userName;
    this.password = password;
    this.street = street;
    this.houseNumber = houseNumber;
    this.zipCode = zipCode;
    this.city = city;
    this.country = country;
    this.firstName = firstName;
    this.lastName = lastName;
  }
}
