export class Vehicle {
  public id: number;
  public brand: string;
  public model: string;
  public licensePlate: string;
  public buildYear: string;
  public color: string;

  public isStolen: boolean;
  public priority: string;
  public stolenSince: Date;

  public carTracker: string;
  public isBeingTracked: boolean = false;

  public _citizenServiceNumber: string;

  constructor(licensePlate: string, isStolen: boolean, priority: string, stolenSince: Date) {
    this.licensePlate = licensePlate;
    this.isStolen = isStolen;
    this.stolenSince = stolenSince;
    this.priority = priority;


  }


  toString(): string {
    return 'id: ' + this.id
      + '  brand: ' + this.brand
      + '  model: ' + this.model
      + '  licensePlate: ' + this.licensePlate
      + '  buildYear: ' + this.buildYear
      + '  color: ' + this.color;
  }
}
