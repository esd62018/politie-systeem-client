import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuardService } from './services/auth-guard/auth-guard.service';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProfileComponent } from './components/profile/profile.component';
import { LoginComponent } from './components/login/login.component';
import {ReportVehicleComponent} from './components/report-vehicle/report-vehicle.component';
import {LiveMapComponent} from './components/live-map/live-map.component';
import {VehicleOverviewComponent} from './components/vehicle-overview/vehicle-overview.component';
import {VehicleHistoryComponent} from './components/vehicle-history/vehicle-history.component';

const routes: Routes = [
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'dashboard',
    component: DashboardComponent,
    canActivate: [ AuthGuardService ],
    children: [
      {
        path: '',
        redirectTo: '/dashboard/(dashboard:profile)',   // TODO: change profile to default inner route (vehicles?)
        pathMatch: 'full'
      },
      {
        path: 'profile',
        component: ProfileComponent,
        outlet: 'dashboard'
      },
      {
        path: 'reportVehicle',
        component: ReportVehicleComponent,
        outlet: 'dashboard'
      },
      {
        path: 'vehicleOverview',
        component: VehicleOverviewComponent,
        outlet: 'dashboard'
      },
      {
        path: 'liveMap',
        component: LiveMapComponent,
        outlet: 'dashboard'
      },
      {
        path: 'vehicleHistory',
        component: VehicleHistoryComponent,
        outlet: 'dashboard'
      }
      // add other here..
    ]
  },
  {
    path: '',
    redirectTo: '/dashboard',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
