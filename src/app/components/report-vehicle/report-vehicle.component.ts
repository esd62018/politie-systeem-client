import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroupDirective, Validators} from '@angular/forms';
import {AccountService} from '../../services/account/account.service';
import {Vehicle} from '../../models/Vehicle';
import {LoaderService} from '../../services/loader/loader.service';
import {VehicleService} from '../../services/vehicle-service/vehicle-service';
import {Router} from '@angular/router';
import {SnackBarService} from '../../services/snack-bar/snack-bar.service';

@Component({
  selector: 'app-report-vehicle',
  templateUrl: './report-vehicle.component.html',
  styleUrls: ['./report-vehicle.component.scss']
})
export class ReportVehicleComponent implements OnInit {


  LicensePlateFormControl = new FormControl('', [
    Validators.required
  ]);

  PriorityFormControl = new FormControl('', [
    Validators.required
  ]);

  DateFormControl = new FormControl('', [
    Validators.required
  ]);

  public reportedVehicle: Vehicle = null;
  public initialised : boolean = false;
  public priorities = [
    {value: 'LOW', viewValue: 'LOW'},
    {value: 'MEDIUM', viewValue: 'MEDIUM'},
    {value: 'HIGH', viewValue: 'HIGH'},]


  constructor(
    private accountService: AccountService,
    private loaderService: LoaderService,
    private vehicleService: VehicleService,
    private snackbarService: SnackBarService,
    private router: Router) {}

  ngOnInit() {
  }

  postStolenVehicle(vehicle: Vehicle) : void {
    this.loaderService.showLoader();

    this.vehicleService
      .postStolenVehicle(vehicle)
      .subscribe(
        data => this.snackbarService.showSnackBar("Voertuig gerapporteerd!"),
        error => this.handleError(error),
        () => this.onComplete()
      );
  }

  onComplete()   {
    this.loaderService.hideLoader();
    this.initialised = true;

    //this.router.navigate(['/dashboard', { dashboard: ['vehicleOverview'] }]);
  }

  submit(formDirective: FormGroupDirective) : void {
    if (
      this.LicensePlateFormControl.invalid ||
      this.PriorityFormControl.invalid ||
      this.DateFormControl.invalid
    ) {
      return;
    }

    const licensePlate = this.LicensePlateFormControl.value;
    const isStolen = true;
    const priority = this.PriorityFormControl.value;
    const stolenSince = this.DateFormControl.value;

    let vehicle: Vehicle = new Vehicle(licensePlate, isStolen, priority, stolenSince);
    console.log(JSON.stringify(vehicle))

    this.postStolenVehicle(vehicle);
    console.log(vehicle.licensePlate + "----" + vehicle.isStolen + "----" + vehicle.priority + "----" + vehicle.stolenSince + " REPORTED");
    this.resetForm(formDirective)

  }

  resetForm(formDirective: FormGroupDirective) {
    formDirective.resetForm();

    this.LicensePlateFormControl.reset();
    this.PriorityFormControl.reset();
    this.DateFormControl.reset();
  }

  handleError(error: Error){
    this.loaderService.hideLoader();
    if (error.message.includes("501")){
      this.snackbarService.showSnackBar("het opgegeven voertuig is onbekend! Check of je een typo hebt gemaakt", 5000)
    } else {
      this.snackbarService.showSnackBar("het opgegeven voertuig is al bekend bij de Politie", 5000)
    }
  }
}
