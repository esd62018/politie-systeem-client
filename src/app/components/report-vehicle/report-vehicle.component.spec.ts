import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportVehicleComponent } from './report-vehicle.component';

describe('ReportVehicleComponent', () => {
  let component: ReportVehicleComponent;
  let fixture: ComponentFixture<ReportVehicleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportVehicleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportVehicleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
