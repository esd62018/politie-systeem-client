import { Component, OnInit } from '@angular/core';
import {SnackBarService} from '../../services/snack-bar/snack-bar.service';
import {LoaderService} from '../../services/loader/loader.service';
import {MovementService} from '../../services/movement/movement-service';
import * as L from 'leaflet';
import {TransLocation} from '../../models/TransLocation';
import {Vehicle} from '../../models/Vehicle';
import {FormControl, FormGroupDirective, Validators} from '@angular/forms';

@Component({
  selector: 'app-vehicle-history',
  templateUrl: './vehicle-history.component.html',
  styleUrls: ['./vehicle-history.component.scss']
})
export class VehicleHistoryComponent implements OnInit {

  private map: L.Map;
  public defaultPosition = {lat : 50.7383394, lng: 4.4450382, zoom: 8};
  _initialized: boolean = true;

  LicensePlateFormControl = new FormControl('', [
    Validators.required
  ]);

  constructor(private snackbarService: SnackBarService,
              private loaderService: LoaderService,
              private movementService: MovementService) { }

  ngOnInit() {

  }
}
