import { Component, OnInit } from '@angular/core';

import { AccountService } from '../../services/account/account.service';
import {LoaderService} from '../../services/loader/loader.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit {
  constructor(
    private accountService: AccountService,
    private loaderService: LoaderService
  ) { }

  ngOnInit(): void {

  }

  logout(): void {
    // gewoon boef man
    this.loaderService.hideLoader();
    this.accountService.logout();
  }
}
