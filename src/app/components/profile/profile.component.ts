import { Component, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { AccountService } from '../../services/account/account.service';
import { SnackBarService } from '../../services/snack-bar/snack-bar.service';
import { LoaderService } from '../../services/loader/loader.service';
import { Account } from '../../models/Account';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {

  submitError: string = null;

  account: Account = null;

  userNameFormControl = new FormControl('', [
    Validators.required
  ]);

  passwordFormControl = new FormControl('', [
    Validators.required,
    Validators.minLength(6),
  ]);

  constructor(
    private accountService: AccountService,
    private snackBarService: SnackBarService,
    private loaderService: LoaderService
  ) {
    this.account = this.accountService.auth;
  }

  ngOnInit() {
    this.mapAccountToFormControlValues();
  }

  mapAccountToFormControlValues(): void {
    if (!this.account) {
      return;
    }

    const { userName, password } = this.account;

    this.userNameFormControl.setValue(userName);
    this.passwordFormControl.setValue(password);
  }

  submit(): void {
    if (this.userNameFormControl.invalid ||
        this.passwordFormControl.invalid
    ) {
      return;
    }

    const userName: string = this.userNameFormControl.value;
    const password: string = this.passwordFormControl.value;

    const account: Account = new Account({
      id: this.account.id,
      userName,
      password,
    });

    this.loaderService.showLoader();

    this.accountService
      .update(account)
      .subscribe(
        () => {},    // Ignore
        error => this.onError(error),
        () => this.onComplete()
      )
  }

  onError(error: any): void {
    const { status } = error;

    // TODO: better error handling
    if (status && status === 404) {
      this.submitError = 'Invalid userName or password';
    } else {
      this.snackBarService.showOops();
    }

    this.loaderService.hideLoader();
  }

  onComplete(): void {
    this.loaderService.hideLoader();
  }
}
