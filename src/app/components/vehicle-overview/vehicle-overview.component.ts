import { Component, OnInit } from '@angular/core';
import {Vehicle} from '../../models/Vehicle';
import {VehicleService} from '../../services/vehicle-service/vehicle-service';
import {LoaderService} from '../../services/loader/loader.service';
import {OwnerService} from '../../services/owner-service/owner-service';
import {Owner} from '../../models/Owner';
import {SnackBarService} from '../../services/snack-bar/snack-bar.service';

@Component({
  selector: 'app-vehicle-overview',
  templateUrl: './vehicle-overview.component.html',
  styleUrls: ['./vehicle-overview.component.scss']
})
export class VehicleOverviewComponent implements OnInit {

  public _iterator: number = 0;

  public _stolenVehicles: Vehicle[] = [];
  public _ownerVehiclemap : Map<string, Owner> = new Map<string, Owner>();
  public owners: Owner[] = [];

  public initialised : boolean = false;
  public showOwner: boolean  = false;
  public selectedOwner : Owner;

  displayedColumns = ['licensePlate', 'Brand', 'Model', 'buildYear', 'color', 'priority', 'stolenSince', 'info'];


  constructor(
    private vehicleService : VehicleService,
    private loaderService : LoaderService,
    private ownerService : OwnerService,
    private snackbarService: SnackBarService
  ) { }

  ngOnInit() {
    this.getStolenVehicles();
  }

  getStolenVehicles() {
    this.loaderService.showLoader();

    this.vehicleService.getStolenVehicles().subscribe(
      data => {this._stolenVehicles = data},
      error => this.handleError(error),
      () => this.getOwners()
    );
  }

  getOwners(){
    for (let vehicle of this._stolenVehicles) {
      this.getOwner(vehicle.licensePlate)
    }
  }

  getOwner(licensePlate: string) {
    this.loaderService.showLoader();

    this.ownerService.getOwner(licensePlate).subscribe(
      data => this._ownerVehiclemap.set(licensePlate, data),
      error => this.handleError(error),
      () => {this._iterator++, this.onComplete()}
    )
  }


  _parseVehiclesToOwner()  {

    for (let entry of Array.from(this._ownerVehiclemap.entries())) {
      let licensePlate: string = entry[0];
      let owner: Owner = entry[1];
      let vehicle = this._stolenVehicles.find(vehicle => vehicle.licensePlate === licensePlate)

      let vehicleIndex = this._stolenVehicles.indexOf(vehicle)
      vehicle._citizenServiceNumber = owner.citizenServiceNumber;
      this._stolenVehicles[vehicleIndex] = vehicle;

      if (this.owners.length < 1){
        this.owners.push(owner)
      }

      let index : number  = this.owners.findIndex(knownOwner => knownOwner.citizenServiceNumber === owner.citizenServiceNumber)
      if (index === -1) {
        this.owners.push(owner)
      } else {
        this.owners[index].stolenVehicles.push(vehicle);
      }
    }
  }

  showInfo(citizenServiceNumber: String){
    this.selectedOwner = this.owners.find(owner => owner.citizenServiceNumber === citizenServiceNumber);
    console.log(JSON.stringify(this.selectedOwner));
    this.showOwner = true;
  }

  getPriorityColor(priority: String){
    switch (priority){
      case 'HIGH':
        return "red"
        //return 'orange';
      case 'MEDIUM':
        return "orange"
        //return 'blue';
      case 'LOW':
        return "green"
        //return 'purple';
      case 'NONE':
        return 'black';
    }
  }

  getDateString(stolenSince: Date) : string{
    let year = String(stolenSince.getFullYear())
    return year;
  }

  onComplete(){
    if(this._iterator === this._stolenVehicles.length) {
      this._parseVehiclesToOwner();
      this.initialised = true;
      console.log(JSON.stringify(this.owners[0].stolenVehicles))
      this.loaderService.hideLoader();
    }
  }


  handleError(error: Error){
    console.log("[VehicleService] " + error.message);
    this.snackbarService.showSnackBar(error.message, 5000 );
  }
}
