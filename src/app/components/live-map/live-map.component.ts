import { Component, OnInit } from '@angular/core';
import {Vehicle} from '../../models/Vehicle';
import {LoaderService} from '../../services/loader/loader.service';
import {SnackBarService} from '../../services/snack-bar/snack-bar.service';
import {VehicleService} from '../../services/vehicle-service/vehicle-service';
import {MovementService} from '../../services/movement/movement-service';
import {BASE_WS_URL} from '../../utils/Constants';
import {TransLocation} from '../../models/TransLocation';
import {MatTableDataSource} from '@angular/material';
import * as L from 'Leaflet';
import {LeafletKeyboardEvent, LeafletMouseEvent} from 'leaflet';
import {Owner} from '../../models/Owner';

@Component({
  selector: 'app-live-map',
  templateUrl: './live-map.component.html',
  styleUrls: ['./live-map.component.scss']
})
export class LiveMapComponent implements OnInit {


  private map: L.Map;
  public defaultPosition = {lat : 50.7383394, lng: 4.4450382, zoom: 9}
  public _isTracking: boolean = false;
  public _trackedVehicle: string = null;
  public lastPosition : Map<string, TransLocation> = new Map<string, TransLocation>();
  public colorCodes: Map<string, string> = new Map<string, string>();

  _initialized: boolean = true;
  stolenVehicles: Vehicle[] = [];


  displayedColumns = ['licensePlate', 'brand', 'model', 'priority','track', 'focus',  'reportFound'];

  constructor(
    private loaderService : LoaderService,
    private snackbarService: SnackBarService,
    private vehicleService: VehicleService,
    private movementService: MovementService

  ) { }

  ngOnInit() {
    this.getStolenVehicles();
    this.initMap();

  }

  getStolenVehicles() {
    this.loaderService.showLoader();

    this.vehicleService.getStolenVehicles().subscribe(
      data => {this.stolenVehicles = data},
      error => this.handleError(error),
      () => {this.snackbarService.showSnackBar("Done loading stolen vehicles!"), this._parseTrackIDsIntoVehicle(), this._parseColorCodes(), this.initSOcket()}
    );
  }

  initSOcket(){
    this.movementService.initSocket().subscribe(
      data => this.drawPoint(data),
      error => console.log("error getting data: " + error),
      () => {}
    );
  }

  trackVehicle(carTracker: string){
    this.loaderService.showLoader();

    let trackedVehicle = this.stolenVehicles.find(vehicle => vehicle.carTracker === carTracker);
    let vehicleIndex = this.stolenVehicles.indexOf(trackedVehicle);

    this.stolenVehicles[vehicleIndex].isBeingTracked = true;
    console.log(this.movementService.trackedVehicles)
    console.log (trackedVehicle.licensePlate + " wordt nu getracked");


    //give a signal to the backend to start sending translocations for inputted cartracker
    this.movementService.giveTrackingSignal(carTracker).subscribe(
      data => {},
      error => {},
      () => this.snackbarService.showSnackBar("started following cartracker: " + carTracker)
    );

    this.loaderService.hideLoader();

  }

  stopTrackingVehicle(carTracker: string){
    this.loaderService.showLoader();

    let trackedVehicle = this.stolenVehicles.find(vehicle => vehicle.carTracker === carTracker);
    let vehicleIndex = this.stolenVehicles.indexOf(trackedVehicle);

    console.log (trackedVehicle.licensePlate + "wordt nu NIET meer getracked");
    this.stolenVehicles[vehicleIndex].isBeingTracked = false;

    //give jetje a signal that no more translocations about the vehicle have to be sent.
    this.movementService.giveSTOPTrackingSignal(carTracker).subscribe(
      data => {},
      error => {},
      () => this.snackbarService.showSnackBar("stopped following cartracker: " + carTracker)
    );

    if (this.lastPosition.has(carTracker) ) {
      this.drawLastPoint(carTracker);
      this.lastPosition.delete(carTracker);
    }

    this.loaderService.hideLoader();
  }

  reportAsFound(carTracker: string){
    this.loaderService.showLoader();

    let foundVehicle = this.stolenVehicles.find(vehicle => vehicle.carTracker === carTracker);
    let vehicleIndex = this.stolenVehicles.indexOf(foundVehicle);

    console.log("trying to remove vehicle: " + JSON.stringify(foundVehicle));


    //Reporting the vehicle as being found.
    this.vehicleService.reportAsFound(carTracker).subscribe(
      data => {},
      error => this.handleError(error),
      () => {
        this.loaderService.hideLoader(),
          this.snackbarService.showSnackBar("voertuig gevonden!"),
          this._deleteVehicle(vehicleIndex),
          console.log("aantal gestolen voertuigen" + JSON.stringify(this.stolenVehicles.length))
      }
    );


    //Also give jetje a signal that no more translocations about the vehicle have to be sent.
    this.movementService.giveSTOPTrackingSignal(carTracker).subscribe(
      data => {},
      error => {},
      () => this.snackbarService.showSnackBar("stopped following cartracker: " + carTracker)
    );

    if (this.lastPosition.has(carTracker) ) {
      this.drawLastPoint(carTracker);
      this.lastPosition.delete(carTracker);
    }
    this.movementService.removeFromBuffer(carTracker);
    
    this.loaderService.hideLoader();
  }

  _parseTrackIDsIntoVehicle(){
    //Asking the backend which of the stolen vehicles have to be tracked
    this.loaderService.hideLoader();
    console.log("currently tracked vehicles: " + this.movementService.trackedVehicles)
    for (let trackID of this.movementService.trackedVehicles){
      let trackedVehicle = this.stolenVehicles.find(vehicle => vehicle.carTracker === trackID);
      let vehicleIndex = this.stolenVehicles.indexOf(trackedVehicle);
      if (vehicleIndex != -1){
        this.stolenVehicles[vehicleIndex].isBeingTracked = true;
        console.log(trackedVehicle.carTracker + " is being tracked");


      }
    }
    this._initialized = true;
    this.loaderService.hideLoader();
  }

  initMap(){
    //https://github.com/wesdoyle/ng-run-journal/blob/master/src/app/services/map.service.ts

    this.map = L.map('liveMap').setView([this.defaultPosition.lat, this.defaultPosition.lng], this.defaultPosition.zoom);
    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
      attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
      maxZoom: 18,
      id: 'mapbox.streets',
      accessToken: 'pk.eyJ1IjoiYmFzdmFuenV0cGhlbiIsImEiOiJjamlqajRud2QxcGtuM2t1cHFhN2lpZTlyIn0.T9hE14uhYoGAw5lx5afoOQ'
    }).addTo(this.map)


    this.map.on("dblclick ", <LeafletMouseEvent>(e) => {

      //https://github.com/Leaflet/Leaflet/issues/1613
      //TODO: centeren op meest dichtije getrackte voertuig.
      var lat = e.latlng.lat;
      var lng = e.latlng.lng;
      console.log("clicked on: "+  lat + " and  " + lng);
      this.startTrackingClosestVehicle(lat, lng);
      if (this._isTracking) {
        this.snackbarService.showSnackBar("Het dichtbijzijndste voertuig wordt LIVE-gevolgd")
      }
    });

    this.map.on("keypress", <LeafletKeyboardEvent>(e)=>{
      // On keypress, go to default position
      this._isTracking = false;
      this.map.setView([this.defaultPosition.lat, this.defaultPosition.lng], this.defaultPosition.zoom);
      this.snackbarService.showSnackBar("Het dichtbijzijndste voertuig NIET MEER LIVE-gevolgd")
    })
    this.map.doubleClickZoom.disable();

  }

  drawPoint(translocation: TransLocation){
    //let foundVehicle = this.stolenVehicles.find(vehicle => vehicle.carTracker === translocation.serialNumber);
    console.log("got: " + translocation.serialNumber + " LAT: " + translocation.lat + " LON: " + translocation.lon + " CountryCode " + translocation.countryCode);

    if (this.lastPosition.has(translocation.serialNumber)) {
      let translocationOld = this.lastPosition.get(translocation.serialNumber);
      console.log("got old translocation");
      L.polyline([[translocationOld.lat, translocationOld.lon], [translocation.lat, translocation.lon]], {
        color: this.colorCodes.get(translocation.serialNumber)
      }).addTo(this.map);
    }
    this.lastPosition.set(translocation.serialNumber, translocation);
    console.log(this.lastPosition.size)

    L.circle([translocation.lat,translocation.lon], {
      color: this.colorCodes.get(translocation.serialNumber),
      fillColor: this.colorCodes.get(translocation.serialNumber),
      fillOpacity: 0.5,
      radius: 25
    }).addTo(this.map);

    if (this._isTracking && this._trackedVehicle === translocation.serialNumber) {
      this.map.setView([translocation.lat, translocation.lon], 16);
    }
  }

  drawLastPoint(serialNumber: string){
    let finalTranslocation = this.lastPosition.get(serialNumber);

    L.marker([finalTranslocation.lat, finalTranslocation.lon], {
      opacity: 1,
      alt: 'stopped tracking'
    }).addTo(this.map)
  }

  startTrackingClosestVehicle(lat: number, lon: number){

    console.log("dikke lul")
    var mindif = 99999;
    var closest;

    if (this.lastPosition.size > 0 ){
      for (let entry of Array.from(this.lastPosition.entries())) {
        let key = entry[0];
        let value = entry[1];
        var dif = this.PythagorasEquirectangular(lat, lon, value.lat, value.lon)

        if (dif < mindif ){
          closest = key;
          mindif = dif;
          this._trackedVehicle = key;
          this._isTracking = true;
          console.log(this._trackedVehicle);
        }
      }
    }
  }

  Deg2Rad(deg) {
    return deg * Math.PI / 180;
  }

  PythagorasEquirectangular(lat1, lon1, lat2, lon2) {
    lat1 = this.Deg2Rad(lat1);
    lat2 = this.Deg2Rad(lat2);
    lon1 = this.Deg2Rad(lon1);
    lon2 = this.Deg2Rad(lon2);
    var R = 6371; // km
    var x = (lon2 - lon1) * Math.cos((lat1 + lat2) / 2);
    var y = (lat2 - lat1);
    var d = Math.sqrt(x * x + y * y) * R;
    return d;
  }

  _getPriorityColor(priority: String) : string {
    switch (priority){
      case 'HIGH':
        return 'red';
      case 'MEDIUM':
        return 'orange';
      case 'LOW':
        return 'green';
      case 'NONE':
        return 'black';
    }
  }

  _parseColorCodes(){
    for (let vehicle of this.stolenVehicles){
      console.log("called)")
      this.colorCodes.set(vehicle.carTracker, this._getPriorityColor(vehicle.priority));
    }

    console.log(this.colorCodes)
  }

  _deleteVehicle(vehicleIndex: number){
    if (vehicleIndex > -1) {
      this.stolenVehicles.splice(vehicleIndex, 1);
    }
  }

  setFocus(carTracker : string, focus: boolean){
    if (!focus){
      this._trackedVehicle = carTracker;
      this._isTracking = true;
    } else {
      this._isTracking = false;
      this._trackedVehicle = "";
      this.map.setView([this.defaultPosition.lat, this.defaultPosition.lng], this.defaultPosition.zoom);

    }
  }

  handleError(error: Error){
    console.log("[VehicleService] " + error.message);
    this.snackbarService.showSnackBar(error.message, 5000 );
    this.loaderService.hideLoader();
  }
}
